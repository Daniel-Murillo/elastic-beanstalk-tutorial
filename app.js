var express = require('express');
var morgan = require('morgan');

var app = express();
var server = require('http').Server(app);
port = process.env.PORT || 8080;

// Capturar todas las otras rutas y devolver el archivo de índice
app.get('/', (req, res) => {
  res.send("hello world!!");
});

// morgan registra las solicitudes en la consola
app.use(morgan('dev'));

server.listen(port);
console.log('App running at http://localhost:' + port);