# Docker Instructions:

* Para crear una imagen de la aplicación docker, en su terminal, navegue hasta el directorio raíz de la aplicación y ejecute: `docker build -t eb-tutorial .`
* Para ejecutar un contenedor docker de la imagen que acaba de crear, ejecute el comando: `docker run -d -p 8080: 8080 --name eb-tutorial eb-tutorial`
* Usa el comando `docker ps` para asegurarte de que tu contenedor está funcionando

* Si está en Mac o Linux, navegue a http: // localhost: 8080 en su navegador para ver su aplicación en ejecución.
* Si está en Windows, navegue a http: // \ <** el docker de puerto se ejecuta en ** \>: 8080 en su navegador para ver su aplicación en ejecución.