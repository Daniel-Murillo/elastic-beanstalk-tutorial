FROM node:alpine

# Crear directorio de aplicación
WORKDIR /usr/src/app

# Instalar dependencias
COPY package.json .

RUN npm install

# Fuente de la aplicación
COPY . .

EXPOSE 8080

CMD [ "npm", "start" ]